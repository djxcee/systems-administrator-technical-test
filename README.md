# Performio Systems Administrator Technical Test
We would like you to solve a series of technical challenges which cover a few points of interest. This is an opportunity to show off your approach to problem solving.


# Question 1
You can execute this command using a Python IDE.  The output is very board and not clean.  Sorry about this!


# Question 2
Using grep command can quickly extract the values needed from a json file.
I am sure there are more delicate ways to extract but depends on the users environment.


# Question 3
In this, I've setup it up a Nginx webserver and configured the server conf to include different local path and to force https:// redirection.  Another conf file was created to add a basic auth to /admin/


# Question 4
We should setup a firewall that will server traffic instead of directly from a EC2 instance.  There are a variey of solutions to choose from such as PFsense.
I would not allow Port 80 traffic and force users to HTTPS (443) by default.  Ports 3389 & 22 should not be used on a public facing infrastracture.  It can be bypassed by brute force or exploits.  
I believe the best way to access the VPC would be through VPN.
I would setup a fowarding server in a DMZ to assist with preventing instrusions though it may be more expensive.  


# Given more time, explain what you would do to improve your submission.
If more time were given, I would probably slim-line some of the codes (Especially the first one) since it's pulling the API requests twice instead of once and format the data in a cleaner matter.  I would also take a look at other API Providers and commands that can be used.
Hope I did well enough.  Please let me know if you have further questions or concerns.