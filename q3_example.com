server {
    root /var/www/;
    index index.html index.htm index.nginx-debian.html;
    
    listen 80;
    server_name example.com www.example.com;
    return 301 https://$server_name$request_uri;
}