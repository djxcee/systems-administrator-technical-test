from datetime import datetime
import requests, pytz

# define urls
api_costamesa = "http://api.openweathermap.org/data/2.5/weather?appid=f2214ca03eeb847fc36ff16c0acdbb18&q=costa%20mesa,us"
api_melbourne = "http://api.openweathermap.org/data/2.5/weather?appid=f2214ca03eeb847fc36ff16c0acdbb18&q=melbourne,aus"

# pull data
json_data1 = requests.get(api_costamesa).json()
json_data2 = requests.get(api_melbourne).json()

# format data for costa mesa & melbourne
format_weather1 = json_data1['main']['temp']

# format data for melbourne
format_weather2 = json_data2['main']['temp']

# convert kelvin to F
a = 32 + ((format_weather1 - 273.15) * (5.0/9.0))
b = 32 + ((format_weather2 - 273.15) * (5.0/9.0))

# get timez from pytz
costamesa = pytz.timezone('America/Los_Angeles')
costamesa_time = datetime.now(costamesa)
melbourne = pytz.timezone('Australia/Melbourne')
melbourne_time = datetime.now(melbourne)


# display results
print("Costa Mesa")
print(a)
print(costamesa_time)
print ("\n")
print("Melbourne")
print(b)
print(melbourne_time)
